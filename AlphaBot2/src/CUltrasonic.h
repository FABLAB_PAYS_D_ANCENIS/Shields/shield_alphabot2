#ifndef CULTRASONIC_H
#define CULTRASONIC_H

#include "generic.h"
#include "LibTimer.h"
#include "COledScreen.h"
#include "MeanFilterLib.h"

#define ECHO   2
#define TRIG   3

#define UltraSonicKFactor 2

class CUltrasonic
{
    public:
        CUltrasonic(COledScreen *);
        int getDistance(void);
        void task (void);

    private:
        double filteredTime;
        float Fdistance;
        uint16 filteredDistance;
        COledScreen *m_OledScreen;
        CLibTimer * timerDisplay;
        uint16 deltaTime;
        bool bStep;
        uint8 errorCounter;
        
};

#endif // CULTRASONIC_H
