#include "CUltrasonic.h"

MeanFilter<long> meanFilter(10);

/*****************************************************************************
 * Function	: CSimpleLed                                                     *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CUltrasonic::CUltrasonic(COledScreen *OledScreen)
{
    // Define the ultrasonic echo input pin
    pinMode(ECHO, INPUT);    
    pinMode(TRIG, OUTPUT); 
    
    // init filtered value
    filteredTime = 0;
    timerDisplay = new CLibTimer();
    timerDisplay->start();
    deltaTime = 20;
    filteredDistance = 0;
    
    m_OledScreen = OledScreen;  
    bStep = 0;
    errorCounter = 0;
}

/*****************************************************************************
 * Function	: switchOnBuzzer                                                 *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
int CUltrasonic::getDistance(void)
{
    return (int)(filteredDistance);
}

/*****************************************************************************
 * Function  : task                                                          *
 *...........................................................................*
 * Description : Cycle time max: 7 ms @ 100cm                                *
 *****************************************************************************/
void CUltrasonic::task(void)
{
    String strText;
  
    
      // set trig pin low 2μs
      digitalWrite(TRIG, LOW);   
      delayMicroseconds(2);
      
      // set trig pin 10μs , at last 10us 
      digitalWrite(TRIG, HIGH);  
      delayMicroseconds(10);
      
      // set trig pin low
      digitalWrite(TRIG, LOW);    
      
      // Read echo pin high level time(us)
      Fdistance = pulseIn(ECHO, HIGH, 8700);

      if(Fdistance != 0)
      {
        // Filter
        filteredTime = ((double)(filteredTime)*UltraSonicKFactor + Fdistance) / ((double)(UltraSonicKFactor) + 1);
        //filteredTime = Fdistance;
        filteredTime = meanFilter.AddValue(Fdistance);
  
        // 34cm / ms emit + reception
        filteredDistance= filteredTime/58;
        // Y m=（X s*344）/2
        // X s=（ 2*Y m）/344 ==》X s=0.0058*Y m ==》cm = us /58
        
        if(filteredDistance < 2) filteredDistance = 2;
        if(filteredDistance > 150) filteredDistance = 150;

        errorCounter = 0;
      }
      else
      {
          errorCounter++;
          if(errorCounter > 10)
          {
              filteredDistance = 150;
          }
      }
      if(bStep == 0)
    {
    }
    else
    {
        // Display distance
        if(errorCounter > 10)
        {
            strText = String("---"); 
        }
        else
        {
           strText = String(int32(filteredDistance)); 
           if (strText.length() == 1) strText = " " + strText;
           if (strText.length() == 2) strText = " " + strText;
        }
        
        strText += " cm";
        m_OledScreen->setText(strText,5,0);
        
        deltaTime = 40;
        timerDisplay->start();
    }

    m_OledScreen->setData(0,(uint8_t)(filteredDistance));

    bStep ^= 1;
}
