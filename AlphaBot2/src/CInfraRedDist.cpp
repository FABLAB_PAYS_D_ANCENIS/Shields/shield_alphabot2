#include "CInfraRedDist.h"

typedef struct { uint16_t x; uint16_t y; } coord_t;

coord_t c[23] = {
{  177 , 0 },
{ 450 , 5 },
{ 500 , 6 },
{ 549 , 7 },
{ 611 , 8 },
{ 700 , 10  },
{ 942 , 15  },
{ 1116  , 20  },
{ 1234  , 25  },
{ 1323  , 30  },
{ 1385  , 35  },
{ 1432  , 40  },
{ 1457  , 45  },
{ 1496  , 50  },
{ 1521  , 55  },
{ 1538  , 60  },
{ 1553  , 65  },
{ 1566  , 70  },
{ 1575  , 75  },
{ 1584  , 80  },
{ 1590  , 85  },
{ 1597  , 90  },
{ 1607  , 100 }
};

/*****************************************************************************
 * Function    : CInfraRedDist                                               *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CInfraRedDist::CInfraRedDist(COledScreen *OledScreen)
{
    bInitDone = false;
    m_OledScreen = OledScreen;
    readStep = 0;

    m_OledScreen->setInverseFont(1);
    
    ads.setGain(GAIN_TWOTHIRDS);
    if (!ads.begin()) 
    {
        bInitDone = false;
    }
    else
    {
        m_OledScreen->setText("Dist",0,3);
        m_OledScreen->setText("Dist",12,3);
        bInitDone = true;
    }

    m_OledScreen->setInverseFont(0);

    for(int i=0; i<4; i++)
    {
        sensorValues[i] = 0;
    }
}

/*****************************************************************************
 * Function    : getInfraRedDist                                             *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
uint16_t CInfraRedDist::getInfraRedDist(int sensor)
{
    return sensorValues[sensor]; 
}

/*****************************************************************************
 * Function  : interpolate                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
uint16_t CInfraRedDist::interpolate(uint16_t x)
{
    int i;
    
    if(x < c[0].x) return c[0].y;
    if(x > c[22].x) return c[22].y;

    for( i = 0; i < 23-1; i++ )
    {
        if ( c[i].x <= x && c[i+1].x >= x )
        {
            double diffx = x - c[i].x;
            double diffn = c[i+1].x - c[i].x;

            return (uint16_t)(c[i].y + ( c[i+1].y - c[i].y ) * diffx / diffn); 
        }
    }

    return 100;
}

/*****************************************************************************
 * Function    : task                                                        *
 *...........................................................................*
 * Description : Cycle time max: 6 ms                                        *
 *****************************************************************************/
void CInfraRedDist::task(void)
{
    uint8_t u8CoordX, u8CoordY;
    uint16_t u16Res = 100;

    if(bInitDone)
    {
        for(int i=0; i<4; i++)
        {
            adc0 = ads.readADC_SingleEnded(i);
            u16Res = interpolate(adc0);
            sensorValues[i] = 100-u16Res;
            
          if(readStep == i)
          {
            if(i == 1) {u8CoordX=0; u8CoordY=4;}
            if(i == 2) {u8CoordX=0; u8CoordY=5;}
            if(i == 3) {u8CoordX=12; u8CoordY=5;}
            if(i == 0) {u8CoordX=12; u8CoordY=4;}
    
            
            
            // Display distance
            String strText = String(int32(sensorValues[i])); 
            while(strText.length() < 3) strText = " " + strText;
            strText += "%";
            m_OledScreen->setText(strText,u8CoordX,u8CoordY);
          }

        }
        readStep++;
        if(readStep > 3) readStep = 0;
    }
    m_OledScreen->setData(4,(uint8_t)(sensorValues[0]));
    m_OledScreen->setData(3,(uint8_t)(sensorValues[1]));
    m_OledScreen->setData(5,(uint8_t)(sensorValues[2]));
    m_OledScreen->setData(6,(uint8_t)(sensorValues[3 ]));
}
