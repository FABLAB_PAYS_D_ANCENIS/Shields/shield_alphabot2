#ifndef LIBTIMER_h
#define LIBTIMER_h

#include "generic.h"

/*----------------------------------------------------------------------------
  Constant data
  #define cCMPConstantName   ((tType) ConstantValue)
-----------------------------------------------------------------------------*/
#define cTOSTimTimerTickMs     1

/*----------------------------------------------------------------------------
  Exported Macros
  #define mCMPMacroName   (MacroDefinition)
-----------------------------------------------------------------------------*/
#define mTOSTimConvMsInTimerTick(x)   ((uint32) ( (x) / cTOSTimTimerTickMs))
#define mTOSTimConvTimerTickInMs(x)   ((uint32) ( (x) * cTOSTimTimerTickMs))


class CLibTimer
{
  public:
    CLibTimer();
    void init(void);
    void start(void);
    bool isElapsed(uint32 time);
    
  private:
    // Data
    uint32 u32Timer;

    // Methods
    

};
#endif
