#ifndef CINFRAREDLINE_H
#define CINFRAREDLINE_H

#include "generic.h"
#include "LibTimer.h"
#include "TRSensors.h"
#include "COledScreen.h"

struct batteryCapacity
{
  uint16 voltage;
  uint8 capacity;
};

const batteryCapacity remainingCapacity[] = {
  4000  , 100 ,
  3600  , 50  ,
  3400  , 10  ,
  3200  , 0 
};

const int ncell = sizeof(remainingCapacity) / sizeof(struct batteryCapacity);

class CInfraRedLine
{
    public:
        CInfraRedLine(COledScreen *);
        uint16 getInfraRed(int dir);
        uint16 getBatteryVoltage(void);
        uint16 getBatteryCapacity(void);
        void task(void);

    private:
        TRSensors *trs;
        uint16 sensorValues[6];
        uint16 batteryVoltage;
        uint16 oldBatteryVoltage;
        uint16 batteryCapacity;
        uint16 oldBatteryCapacity;
        uint16 lastDisplayBattery;
        uint16 lastDisplayCapacity;
        COledScreen *m_OledScreen;
        CLibTimer * timerDisplay;
        uint16 deltaTime;
        uint8 sensorDispValues[5];
        uint8 readStep;
};

#endif // CINFRAREDLINE_H
