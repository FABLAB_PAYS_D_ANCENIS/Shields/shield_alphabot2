#include "CInfraRedObstacle.h"

/*****************************************************************************
 * Function    : CInfraRedObstacle                                           *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CInfraRedObstacle::CInfraRedObstacle(TwoWire *Wirep, COledScreen *OledScreen)
{
    u8Addr = 0x20;
    storeWire = Wirep;
    
    //set Pin High
    PCF8574Write(0xC0 | PCF8574Read());   
    
    bBpLeftOldState = 0;
    bBpRightOldState = 0;
    
    m_OledScreen = OledScreen;
    bInfraRedLeft = 0;
    bInfraRedRight = 0;
}

/*****************************************************************************
 * Function    : switchOnBuzzer                                                 *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
bool CInfraRedObstacle::getInfraRed(int dir)
{
    if(dir == 1)
    {
        return bInfraRedRight;
    }
    else
    {
        return bInfraRedLeft;
    }
}

/*****************************************************************************
 * Function    : switchOnBuzzer                                                 *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
bool CInfraRedObstacle::getEdgeInfraRed(int dir)
{
    bool bCurValue, bReturn;
    
    if(dir == 1)
    {
        bCurValue = getInfraRed(dir);
        if((bCurValue == 1) && (bBpRightOldState == 0)) 
            bReturn = 1;
        else
            bReturn = 0;
        bBpRightOldState = bCurValue;
    }
    else
    {
        bCurValue = getInfraRed(dir);
        if((bCurValue == 1) && (bBpLeftOldState == 0)) 
            bReturn = 1;
        else
            bReturn = 0;
        bBpLeftOldState = bCurValue;
    }
    
    return bReturn;
}

/*****************************************************************************
 * Function    : PCF8574Write                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CInfraRedObstacle::PCF8574Write(byte data)
{
    storeWire->beginTransmission(u8Addr);
    storeWire->write(data);
    storeWire->endTransmission(); 
}

/*****************************************************************************
 * Function    : PCF8574Read                                                    *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
byte CInfraRedObstacle::PCF8574Read()
{
    int data = -1;
    storeWire->requestFrom((int)(u8Addr), (int)(1));
    if(storeWire->available()) 
    {
        data = storeWire->read();
    }
    return data;
}

/*****************************************************************************
 * Function    : task                                                        *
 *...........................................................................*
 * Description : Cycle time max: 3 ms                                        *
 *****************************************************************************/
void CInfraRedObstacle::task(void)
{
    uint8 bReadValue;
    uint8_t tilesOn[8]  = { 0x0C,0x02,0x69,0xF5,0xF5,0x69,0x02,0x0C};
    uint8_t tilesOff[8] = { 0x00,0x00,0x60,0x90,0x90,0x60,0x00,0x00};
  
    //set Pin High
    PCF8574Write(0xC0 | PCF8574Read()); 
    bReadValue = PCF8574Read();
    
    bInfraRedRight = (bReadValue & 0x40) == 0;
    bInfraRedLeft  = (bReadValue & 0x80) == 0;

    if (bInfraRedRight)
    {
        m_OledScreen->drawTile(15, 0, 1, tilesOn);
    }
    else
    {
        m_OledScreen->drawTile(15, 0, 1, tilesOff);
    }

    if (bInfraRedLeft)
    {
        m_OledScreen->drawTile(0, 0, 1, tilesOn);
    }
    else
    {
        m_OledScreen->drawTile(0, 0, 1, tilesOff);
    }    
    m_OledScreen->setData(1,(uint8_t)(bInfraRedLeft));
    m_OledScreen->setData(2,(uint8_t)(bInfraRedRight));
}
