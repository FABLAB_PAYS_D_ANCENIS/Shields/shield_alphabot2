#ifndef CInfraRedDist_H
#define CInfraRedDist_H

#include "generic.h"
#include "Adafruit_ADS1X15.h"
#include "COledScreen.h"
#include <Wire.h>

class CInfraRedDist
{
    public:
        CInfraRedDist(COledScreen *);
        uint16_t getInfraRedDist(int sensor);
        void task(void);

    private:
        Adafruit_ADS1015 ads;
        int16_t adc0, adc1, adc2, adc3;
        uint16_t interpolate(uint16_t x);
        COledScreen *m_OledScreen;
        bool bInitDone;
        uint8 sensorValues[4];
        uint8 readStep;
};

#endif // CInfraRedDist_H
