#ifndef CBUTTON1BP_H
#define CBUTTON1BP_H

#include "generic.h"

#define cBpOk   0


class CButton1Bp
{
public:
    CButton1Bp();
    bool getValue(void);
    bool getEdgeValue(void);

private:
    bool m_u8PinButton;
    uint32 u32TempoBpOk;
    uint32 u32TempoBpOkEdge;
    bool bBpOkstate;
    bool bBpOkOldState;
    bool fabDigitalRead(void);
};

#endif // CBUTTON1BP_H
