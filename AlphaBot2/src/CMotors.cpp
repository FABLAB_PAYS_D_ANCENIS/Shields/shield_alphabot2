#include "CMotors.h"

/*****************************************************************************
 * Function  : CMotors                                                        *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CMotors::CMotors(COledScreen *OledScreen)
{
    pinMode(PWMA,OUTPUT);                     
    pinMode(AIN2,OUTPUT);      
    pinMode(AIN1,OUTPUT);
    pinMode(PWMB,OUTPUT);       
    pinMode(AIN1,OUTPUT);     
    pinMode(AIN2,OUTPUT);  
    analogWrite(PWMA,0);
    analogWrite(PWMB,0);
    digitalWrite(AIN2,HIGH);
    digitalWrite(AIN1,LOW);
    digitalWrite(BIN1,HIGH); 
    digitalWrite(BIN2,LOW); 
    
    rightSpeed = 0;
    leftSpeed = 0;
    
    timerRightDisplay = new CLibTimer();
    timerRightDisplay->start();
    deltaRightTime = 150;
    
    timerLeftDisplay = new CLibTimer();
    timerLeftDisplay->start();
    deltaLeftTime = 150;
    
    m_OledScreen = OledScreen;
    m_OledScreen->setInverseFont(1);
    m_OledScreen->setText("Moteur",5,7);
    m_OledScreen->setInverseFont(0);

    bStep = 0;
}

/*****************************************************************************
 * Function  : driveMotorRight                                                *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CMotors::driveMotorRight(int dir, int speed)
{
    uint8 u8Speed = 0;
    
    if(dir == -1)
    {
        digitalWrite(BIN1,HIGH); 
        digitalWrite(BIN2,LOW); 
    }
    else
    {
        digitalWrite(BIN1,LOW); 
        digitalWrite(BIN2,HIGH); 
    }
    
    if(speed != 0)
    {
        u8Speed = map(speed, 0, 100, 30, 130);
    }
    
    analogWrite(PWMB,u8Speed);
    
    rightSpeed = speed;
    
}

/*****************************************************************************
 * Function  : driveMotorLeft                                                 *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CMotors::driveMotorLeft(int dir, int speed)
{
    uint8 u8Speed = 0;
    
    if(dir == -1)
    {
        digitalWrite(AIN1,HIGH); 
        digitalWrite(AIN2,LOW); 
    }
    else
    {
        digitalWrite(AIN1,LOW); 
        digitalWrite(AIN2,HIGH); 
    }
    
    if(speed != 0)
    {
        u8Speed = map(speed, 0, 100, 30, 130);
    }
    
    analogWrite(PWMA,u8Speed);
    
    leftSpeed = speed;  
}

/*****************************************************************************
 * Function  : stopMotorRight                                                 *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CMotors::stopMotorRight(void)
{
    digitalWrite(BIN1,HIGH); 
    digitalWrite(BIN2,LOW); 
    
    analogWrite(PWMB,0);
    
    leftSpeed = 0;
}

/*****************************************************************************
 * Function  : stopMotorLeft                                                  *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CMotors::stopMotorLeft(void)
{
  digitalWrite(AIN1,HIGH); 
  digitalWrite(AIN2,LOW); 
  
  analogWrite(PWMA,0);

  rightSpeed = 0;
}

/*****************************************************************************
 * Function  : task                                                          *
 *...........................................................................*
 * Description : Cycle time max: 5 ms                                        *
 *****************************************************************************/
void CMotors::task(void)
{
    String strText;
    
    // Display 
    if(bStep == 0)
    {
        strText = String(int32(rightSpeed)); 
        if (strText.length() == 1) strText = " " + strText;
        if (strText.length() == 2) strText = " " + strText;
        strText += "%";
        m_OledScreen->setText(strText,12,7);
        
        deltaRightTime = 180;
        timerRightDisplay->start();
    }
    else
    {
        strText = String(int32(leftSpeed)); 
        if (strText.length() == 1) strText = " " + strText;
        if (strText.length() == 2) strText = " " + strText;
        strText += "%";
        m_OledScreen->setText(strText,0,7);
        
        deltaLeftTime = 180;
        timerLeftDisplay->start();
    }

    m_OledScreen->setData(7,(uint8_t)(leftSpeed));
    m_OledScreen->setData(8,(uint8_t)(rightSpeed));

    bStep ^= 1;
}
