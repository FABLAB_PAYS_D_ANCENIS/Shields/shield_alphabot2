#ifndef CSYSTEMALPHABOT2_H
#define CSYSTEMALPHABOT2_H

#include "generic.h"
#include "LibTimer.h"
#include "COledScreen.h"

class CSystemAlphaBot2
{
    public:
        CSystemAlphaBot2(COledScreen *);
        void configure(void);
        void check(void);
        void disableErrors(void);
        void error(uint8 u8Err);
        void task(void);

    private:
        bool bErrorDisabled;
        COledScreen *m_OledScreen;
        CLibTimer * timerDisplay;
        uint8 u8Step;
        bool bDisp;
        bool bDir;
};

#endif // CSYSTEMALPHABOT2_H
