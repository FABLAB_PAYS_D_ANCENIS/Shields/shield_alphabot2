#ifndef CINFRAREDOBSTACLE_H
#define CINFRAREDOBSTACLE_H

#include "generic.h"
#include "COledScreen.h"
#include "LibTimer.h"
#include <Wire.h>

class CInfraRedObstacle
{
    public:
        CInfraRedObstacle(TwoWire *Wirep, COledScreen *);
        bool getInfraRed(int dir);
        bool getEdgeInfraRed(int dir);
        void task(void);

    private:
        uint8 u8Addr;
        void PCF8574Write(byte data);
        byte PCF8574Read();
        TwoWire *storeWire;
        
        bool   bBpRightOldState;
        bool   bBpLeftOldState;
        COledScreen *m_OledScreen;
        CLibTimer * timerDisplay;
        bool bInfraRedLeft;
        bool bInfraRedRight;
};

#endif // CINFRAREDOBSTACLE_H
