#include "CSystemAlphaBot2.h"


/*****************************************************************************
 * Function    : CSystemAlphaBot2                                              *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CSystemAlphaBot2::CSystemAlphaBot2(COledScreen *OledScreen)
{
    // Init pcb rev to 0 = unknown
    //m_u8PcbRevision = 0;
    bErrorDisabled  = false;

    // Detect Pcb
    configure();
    
    m_OledScreen = OledScreen;
    /*m_OledScreen->setInverseFont(1);
    m_OledScreen->setText("Proc",2,1);
    m_OledScreen->setInverseFont(0);*/

    uint8_t tilesicon[8] = { 0x24,0x7E,0xC3,0x5A,0x5A,0xC3,0x7E,0x24};
    m_OledScreen->drawTile(2, 1, 1, tilesicon);

    uint8_t tilesstart[8] = { 0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0x81};
    m_OledScreen->drawTile(3, 1, 1, tilesstart);

    uint8_t tilesstop[8] = { 0x81,0xFF,0x00,0x00,0x00,0x00,0x00,0x00};
    m_OledScreen->drawTile(12, 1, 1, tilesstop);

    u8Step = 0;
    bDisp = 1;
    bDir = 0;
}

/*****************************************************************************
 * Function    : configure                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CSystemAlphaBot2::task(void)
{
    if(bDisp)
    {
        uint8_t tilesoff[8] = { 0x81,0x81,0x81,0x81,0x81,0x81,0x81,0x81};
        m_OledScreen->drawTile(u8Step+4, 1, 1, tilesoff);
        if(bDir == 0)
        {
            u8Step++;
            if(u8Step == 7) bDir = 1;
        }
        else
        {
            u8Step--;
            if(u8Step == 0) bDir = 0;
        }
        uint8_t tileson[8] = { 0x81,0xBD,0xBD,0xBD,0xBD,0xBD,0xBD,0x81};
        m_OledScreen->drawTile(u8Step+4, 1, 1, tileson);
    }
    
    bDisp = bDisp ^ 1;

    check();
}

/*****************************************************************************
 * Function    : configure                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CSystemAlphaBot2::configure(void)
{
   //m_u8PcbRevision = 1;
}

/*****************************************************************************
 * Function    : configure                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CSystemAlphaBot2::disableErrors(void)
{
    bErrorDisabled = true;
}

/*****************************************************************************
 * Function    : configure                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CSystemAlphaBot2::check(void)
{
    // Check if error is disabled
    if(bErrorDisabled == true)
        return;

    // On Rev 1 pcb
    //if(m_u8PcbRevision == 1)
    {

    }
    // On Rev 2 pcb
    //else if(m_u8PcbRevision == 2)
    {

    }
}

/*****************************************************************************
 * Function    : configure                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CSystemAlphaBot2::error(uint8 u8Err)
{
    if(u8Err == 1)
    {
        
    }
    else if(u8Err == 2)
    {
        
    }

    // Stop processing
    while(1){}
}
