#include "CInfraRedLine.h"

/*****************************************************************************
 * Function    : CSimpleLed                                                     *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CInfraRedLine::CInfraRedLine(COledScreen *OledScreen)
{
    trs = new TRSensors();

    timerDisplay = new CLibTimer();
    timerDisplay->start();
    deltaTime = 90;
    
    m_OledScreen = OledScreen;
    m_OledScreen->setInverseFont(1);
    m_OledScreen->setText("Batt",6,3);
    //m_OledScreen->setText("Capt. lignes (%)",0,2);
    m_OledScreen->setInverseFont(0);

    trs->AnalogRead((unsigned int*)(sensorValues));
    oldBatteryVoltage = ((uint16)(((uint32)(sensorValues[5]))*964/100));
    oldBatteryCapacity = getBatteryCapacity();
    lastDisplayBattery = 0;
    lastDisplayCapacity = 0;
    readStep = 0;

    task();
}

/*****************************************************************************
 * Function    : switchOnBuzzer                                                 *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
uint16 CInfraRedLine::getInfraRed(int dir)
{
    int newDir;
    
    if(dir < 1) newDir = 1;
    else if (dir > 5) newDir = 5;
    else newDir = dir;
    
    return sensorDispValues[newDir-1];
}

/*****************************************************************************
 * Function    : getBatteryVoltage                                           *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
uint16 CInfraRedLine::getBatteryVoltage(void)
{
    return oldBatteryVoltage;
}

/*****************************************************************************
 * Function    : task                                                        *
 *...........................................................................*
 * Description : Cycle time max: 6 ms                                        *
 *****************************************************************************/
void CInfraRedLine::task(void)
{
    String strText;
  
    trs->AnalogRead((unsigned int*)(sensorValues));
    batteryVoltage = ((uint16)(((uint32)(sensorValues[5]))*964/100));

    // filter
    if(abs((int32)(oldBatteryVoltage) - (int32)(batteryVoltage)) > 3000)
      oldBatteryVoltage = batteryVoltage;
    else
     oldBatteryVoltage = (uint16)((((uint32)(oldBatteryVoltage) * 100000) + batteryVoltage) / 100001);
     
    batteryCapacity = getBatteryCapacity();

    // filter
    if(abs(oldBatteryCapacity - batteryCapacity) > 30)
      oldBatteryCapacity = batteryCapacity;
    else
      oldBatteryCapacity = (uint16)((((uint32)(oldBatteryCapacity) * 100000) + batteryCapacity) / 100001);

    for(int i=0; i<5; i++)
    {
        sensorDispValues[i] = min(map(sensorValues[i], 0, 1024, 0, 100), 99);
    }
    
    // Display battery
    if(readStep == 0) 
    {
        if(batteryVoltage != lastDisplayBattery)
        {
            float fVoltage = oldBatteryVoltage;
            fVoltage /= 1000;
            strText = String(fVoltage,1) + String("V");; 
            m_OledScreen->setText(strText,6,4);
            lastDisplayBattery = batteryVoltage;
        }
        
        if(batteryCapacity != lastDisplayCapacity)
        {
            float fVoltage = oldBatteryCapacity;
            strText = String(fVoltage,0) + String("%"); 
            while(strText.length() < 4) strText = " " + strText;
            m_OledScreen->setText(strText,6,5);
            lastDisplayCapacity = batteryCapacity;
        }
    }
    else if(readStep >= 1 && readStep <= 5) 
    {
        /*String strText = String(int32(sensorDispValues[readStep - 1]));
        while(strText.length() < 3) strText = " " + strText;
        m_OledScreen->setText(strText, (readStep - 1) * 3, 3);*/
     }

     readStep++;
     if(readStep > 5) readStep = 0;
}

/*****************************************************************************
 * Function    : getBatteryCapacity                                          *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
uint16 CInfraRedLine::getBatteryCapacity(void)
{
    uint16 voltage;
    uint8 capacity;
    voltage = getBatteryVoltage()/2;
    
    for (int i = 0 ; i < ncell ; i++)
    {
        if ((i>0) && (voltage > remainingCapacity[i].voltage))
        {
            capacity =  map(voltage,remainingCapacity[i-1].voltage,remainingCapacity[i].voltage,remainingCapacity[i-1].capacity,remainingCapacity[i].capacity);
            if(capacity > 100) capacity = 100;
            return capacity;
        }
    }
    return 0;
}
