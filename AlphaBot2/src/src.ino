#include <Arduino.h>
#include <Wire.h>
#include <SoftwareSerial.h>

#include "CMotors.h"
#include "CInfraRedObstacle.h"
#include "CInfraRedDist.h"
#include "CInfraRedLine.h"
#include "COledScreen.h"
#include "CUltrasonic.h"
#include "CSystemAlphaBot2.h"
#include "CButton1Bp.h"
#include "CRadioFreqBot.h"
#include <SPI.h>
#include <SoftwareSerial.h>

double angle_rad = PI/180.0;
double angle_deg = 180.0/PI;
CSystemAlphaBot2 *System;
CUltrasonic *Ultrasonic;
COledScreen *OledScreen;
CInfraRedLine *InfraRedLine;
CInfraRedDist *InfraRedDist;
CInfraRedObstacle *InfraRedObstacle;
CMotors *Motor;
CButton1Bp *Button1Bp;
CRadioFreqBot *RadioFreq;
double Start;
double PuissDroit;
double PuissGauche;
double Ecart;

unsigned int memoMillis;

void setup()
{
    OledScreen = new COledScreen();
	  System = new CSystemAlphaBot2(OledScreen);
    System->configure();
    Wire.begin();

    
    Button1Bp = new CButton1Bp();
    Ultrasonic = new CUltrasonic(OledScreen);
    InfraRedLine = new CInfraRedLine(OledScreen);
    InfraRedLine->task();
    InfraRedDist = new CInfraRedDist(OledScreen);
    InfraRedObstacle = new CInfraRedObstacle(&Wire, OledScreen);
    Motor = new CMotors(OledScreen);
    RadioFreq = new CRadioFreqBot(7,10, OledScreen);
    RadioFreq->init(1);
    RadioFreq->destAddress(0);
    
    Serial.begin(115200);
    Serial.println("Start");
    
    Start = 0;
    PuissDroit = 0;
    PuissGauche = 0;
    Ecart = 0;    
    memoMillis = millis();
}

void loop(){
    
    
    
     if((((Start)==(0))) && (Button1Bp->getEdgeValue())){
        Start = 1;
    }
   if((((Start)==(1))) && (Button1Bp->getEdgeValue())){
        Start = 0;
    }
    if(((Start)==(1))){
        if((InfraRedDist->getInfraRedDist(1)) > (0)){
            PuissDroit = 10;
        }
        else{
          PuissDroit = 20;
        }
        if((InfraRedDist->getInfraRedDist(0)) > (0)){
            PuissGauche = 10;
        }
        else
        {
          PuissGauche = 20;
        }
        Motor->driveMotorRight(1,PuissDroit);
        Motor->driveMotorLeft(1,PuissGauche);
    }
    if(((Start)==(0))){
        Motor->stopMotorRight();
        Motor->stopMotorLeft();
    }
    
    _loop();

    Serial.print("loop time: ");
    Serial.println(millis() - memoMillis);
    memoMillis = millis();
}

void _delay(float seconds){
    long endTime = millis() + seconds * 1000;
    while(millis() < endTime)_loop();
}

void _loop()
{
    System->task();
    Ultrasonic->task();       // Cycle time max: 7 ms @ 100cm
    InfraRedDist-> task();    // Cycle time max: 6 ms
    Motor-> task();           // Cycle time max: 5 ms
    InfraRedObstacle->task(); // Cycle time max: 3 ms   
    RadioFreq->task();
}
