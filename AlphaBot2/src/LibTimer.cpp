#include "LibTimer.h"


/*****************************************************************************
 * Function    : CLibTemp                                                    *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CLibTimer::CLibTimer()
{

    u32Timer = 0; 
    
}

/*****************************************************************************
 * Function    : init                                                        *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CLibTimer::init(void)
{
    u32Timer = 0; 
}

/*****************************************************************************
 * Function    : start                                                       *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CLibTimer::start(void)
{
    u32Timer = millis(); 
}

/*****************************************************************************
 * Function    : isElapsed                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
bool CLibTimer::isElapsed(uint32 time)
{
    // A timer is never equal to 0
    // The 0 value is reserved to the timer stoped
    if( u32Timer == 0 )
    {
        return true;
    }
    else
    {
        uint32 Delay;

        Delay = (uint32)(millis() - u32Timer);

        if( millis() < u32Timer )
        {
            // The 0 value had been "jump" so we must substact 1 to the delay
            Delay -- ;
        }

        if( (Delay >= mTOSTimConvMsInTimerTick(time)) || ( mTOSTimConvMsInTimerTick(time)==0 ) )
        {
            // The timer is stopped.
            u32Timer = 0;
            return true;
        }
        else
        {
            return false;
        }
    }
}
