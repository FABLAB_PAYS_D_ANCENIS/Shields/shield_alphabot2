#ifndef CMotors_H
#define CMotors_H

#include "generic.h"
#include "COledScreen.h"

#define PWMA   6           //Left Motor Speed pin (ENA)
#define AIN2   A0          //Motor-L forward (IN2).
#define AIN1   A1          //Motor-L backward (IN1)
#define PWMB   5           //Right Motor Speed pin (ENB)
#define BIN1   A2          //Motor-R forward (IN3)
#define BIN2   A3          //Motor-R backward (IN4)

class CMotors
{
    public:
        CMotors(COledScreen *);
        void driveMotorLeft(int dir, int speed);
        void driveMotorRight(int dir, int speed);
        void stopMotorLeft(void);
        void stopMotorRight(void);
        void task(void);

    private:
        COledScreen *m_OledScreen;
        uint8 rightSpeed;
        uint8 leftSpeed;
        CLibTimer * timerRightDisplay;
        CLibTimer * timerLeftDisplay;
        uint16 deltaRightTime;
        uint16 deltaLeftTime;
        bool bStep;
};

#endif // CMotors_H
