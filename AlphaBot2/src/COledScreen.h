#ifndef COledScreen_H
#define COledScreen_H

/*#include "Adafruit_GFX.h"
#include "Adafruit_SSD1306.h"*/
#include "generic.h"
#include "U8x8lib.h"
#include "LibTimer.h"
#include "COledScreen.h"
/*#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64
#define OLED_RESET 9
#define OLED_SA0   8*/

  
 
class COledScreen {

    public:
        //Adafruit_SSD1306 *display;
        COledScreen();
        void setBackground(uint8_t red, uint8_t green, uint8_t blue);
        void setFill(uint16 color);
        void setColor(uint16 color);
        void setFont(int font);
        void setStroke(uint8_t red, uint8_t green, uint8_t blue);
        void setText(double val, int16_t x, int16_t y);
        void setText(String strText, int16_t x, int16_t y);
        void setText(const char * val, int16_t x, int16_t y);
        void setInverseFont(bool bInv);
        void setPoint(int16_t x, int16_t y);
        void drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1);
        void setTextSize(int size);
        void clearDisplay(void);
        void launchDisplay(void);
        void circle(int16_t x,int16_t y,int16_t rayon);
        void rect(int16_t x,int16_t y,int16_t longu, int16_t larg);
        void triangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, int16_t x2, int16_t y2);
        void drawTile(uint8_t x, uint8_t y, uint8_t cnt, uint8_t *tile_ptr);
        void setData (uint8_t id, uint8_t data);
        void getBuffer(uint8_t* buffer);

    private:
        uint16 u16Color;
        uint16 u16FillColor;
        U8X8_SSD1306_128X64_NONAME_HW_I2C *u8x8;
        COledScreen *m_OledScreen;
        CLibTimer * timerDisplay;
        uint8_t u8Buffer[10];
};


#endif // COledScreen_H
